# Общие настройки
LINK_PATTERN = r'https:\/\/[\w_-]+(?:(?:\.[\w_-]+)+)[\w.,@?^=%&:\/~+#-]*[\w@?^=%&\/~+#-]'
DATETIME_FORMAT = '%d.%m.%Y_%H:%M'
MAXIMUM_TIMEOUT_IN_MINUTES = 2
MAX_REPEATED_CRUSH_NUMBER = 2

# Flask настройки

APP_SECRET_KEY = 'XXXXXXXXXXXXX'  # секретный ключ Flask

# Авторизация
USER_NAME = 'alert_user'
PASSWORD = 'XXXXXXXXXXXXXXXXXXXXXXXXXXX'

# Таймауты
SMALL_TIMEOUT = 30
LARGE_TIMEOUT = 60

PROCESSED_MESSAGE = 'Alert process'
OK_MESSAGE = 'Alert send'
FAIL_MESSAGE = 'Alert fail'

HOST = '0.0.0.0'
PORT = 9119

# Telegram настройки
BOT_TOKEN = 'XXXXXXXXXX:XXX_XXXXXXXXXXXXXXXXXXXXX_XXXXXXXXX'
CHAT_STRUCTURE = {
    'default': '-XXXXXXXXXXXXX',
    'exporter': {
        'node_exporter': (
            '-XXXXXXXXXXXXX',
        ),
    },
    'service': {
        'OZCH': (
            '-XXXXXXXXXXXXX',
        ),
    },
}

# Настройки Redis
REDIS_HOST = 'portainer.keyauto.ru'
REDIS_PORT = '6379'
REDIS_LOGIN = 'root'
REDIS_PASSWORD = 'M15c45mqXK'

# Настройки для отправки SMS
SMS_URL = 'https://sms-gate.ABC.com/sendSms'
SMS_USER = 'sender'
SMS_PASSWORD = 'XXXXXXXXXXX'

# Номера телефонов для рассылки
PORTAL_PHONE_NUMBER = '+78005553535'
