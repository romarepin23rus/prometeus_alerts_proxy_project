FROM python:3.8.13-alpine3.15

RUN apk update \
    && apk add py-pip bash gcc python3-dev musl-dev libffi-dev openssl-dev unzip git \
    && rm -rf /var/cache/apk/*

RUN git clone https://gitlab.ru/web-solutions/telegram_alerts_bot.git -b main

WORKDIR /telegram_alerts_bot

RUN pip install --upgrade pip && pip install -r requirements.txt

EXPOSE 9119

ENTRYPOINT ["python", "flask_alert.py"]
