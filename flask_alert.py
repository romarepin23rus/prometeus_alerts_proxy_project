from re import findall
from time import sleep
# from datetime import timedelta

from json import loads
from logging import basicConfig
from logging import INFO
from flask import Flask
from flask import request
from flask_basicauth import BasicAuth
from telegram import Bot
from telegram.error import RetryAfter
from telegram.error import TimedOut
from telegram.error import NetworkError
from telegram.parsemode import ParseMode

# from redis_connect import RedisConnect
from settings import LINK_PATTERN
from settings import APP_SECRET_KEY
from settings import USER_NAME
from settings import PASSWORD
from settings import SMALL_TIMEOUT
from settings import LARGE_TIMEOUT
# from settings import MAX_REPEATED_CRUSH_NUMBER
from settings import OK_MESSAGE
from settings import FAIL_MESSAGE
from settings import PROCESSED_MESSAGE
from settings import HOST
from settings import PORT
from settings import BOT_TOKEN
from settings import CHAT_STRUCTURE
# from settings import MAXIMUM_TIMEOUT_IN_MINUTES
from settings import PORTAL_PHONE_NUMBER
# from utils import get_current_date_and_time
from utils import send_sms

APP = Flask(__name__)
APP.secret_key = APP_SECRET_KEY
basic_auth = BasicAuth(APP)

# Authentication conf, change it!
APP.config['BASIC_AUTH_FORCE'] = True
APP.config['BASIC_AUTH_USERNAME'] = USER_NAME
APP.config['BASIC_AUTH_PASSWORD'] = PASSWORD

BOT = Bot(token=BOT_TOKEN)


def send_message(chat_id: str, message: str) -> None:
    """
    Отправляем сообщение с бота.

    :param chat_id: Идентификатор чата в ТГ.
    :param message: Текст сообщения.
    :return: None.
    """

    BOT.sendMessage(chat_id=chat_id, text=message, parse_mode=ParseMode.MARKDOWN, timeout=LARGE_TIMEOUT)
    print(f'Channel: {chat_id}, message: {message} send')


# TODO пока что этот костыль не используется, позже надо рассмотреть целесообразность
# def check_whether_need_send_message(service_identifier: str) -> bool:
#     """
#   Проверяем необходимость отправки сообщения в Telegram при помощи данных об уведомлениях о падениях сервиса из Redis.
#
#
#     :param service_identifier: Незашифрованный идентификатор сервиса.
#     :return: Флаг отправки сообщения в Telegram.
#     """
#     maximum_timeout = timedelta(minutes=MAXIMUM_TIMEOUT_IN_MINUTES)
#     current_time = get_current_date_and_time()
#     redis_connect = RedisConnect()
#     is_worth_send_message = False
#     alert_info = redis_connect.read_value(service_identifier)
#
#     time_of_last_falling = alert_info.get('time_of_last_falling') if alert_info else current_time
#     repeated_crash_number = alert_info['repeated_crash_number'] if alert_info else 1
#     # проверяем сколько времени прошло с последнего уведомления
#     delta = current_time - time_of_last_falling
#     if delta <= maximum_timeout:
#         repeated_crash_number += 1
#         if repeated_crash_number >= MAX_REPEATED_CRUSH_NUMBER:
#             is_worth_send_message = True
#             repeated_crash_number = 0
#     else:
#         repeated_crash_number = 1
#
#     # записываем обновлённые данные в Redis
#     redis_connect.write_value(
#         service_identifier,
#         current_time,
#         repeated_crash_number
#     )
#     return is_worth_send_message


def make_web_solutions_message(alert: dict) -> str:
    """
    Создаём сообщение по шаблону для отдела Веб Решений.
    :param alert: Словарь с данными из POST запроса.
    :return: Сообщение, составленное по шаблону.
    """
    service = alert.get('labels').get('exporter')
    host = alert.get('labels').get('host')
    event = alert.get('annotations').get('summary')
    reason = alert.get('annotations').get('description')
    message = f'''**Service**: - **`{service}`**
    **Host**: - `{host}`
    **Событие**: - `{event}`
    **Причина**: - `{reason}`
    **Ссылка**: - `https://metrics.keyauto.ru/alert`'''
    return message


def make_services_message(alert: dict) -> str:
    """
    Создаём сообщение по шаблону для сервисов.
    :param alert: Словарь с данными из POST запроса.
    :return: Сообщение, составленное по шаблону
    """
    service = alert.get('labels').get('service')
    description = alert.get('annotations').get('description')
    panel_link = findall(LINK_PATTERN, alert.get('annotations').get('Panel_link'))[0]
    summary = alert.get('annotations').get('summary')
    message = f'''**Service**: **`{service}`**
    **Событие**: `{summary}`
    **Описание**: `"{description}"`
    **Панель URL**: `{panel_link}`'''
    return message


@APP.route('/alert', methods=['POST'])
def post_alert_manager():
    print(PROCESSED_MESSAGE)
    content = loads(request.get_data())
    for alert in content.get('alerts'):
        # определяем название сервиса
        service_identifier = f'{alert.get("labels").get("host")}_{alert.get("labels").get("service")}'
        # определяем каналы для рассылки
        notification_channels = []
        is_web_solutions_alert = True
        for label, label_value in alert.get('labels').items():
            if label == 'service' and label_value == 'OZCH':
                is_web_solutions_alert = False
            label_values = CHAT_STRUCTURE.get(label)
            chats_identifiers = label_values.get(label_value) if label_values else None
            if chats_identifiers:
                for chat_id in chats_identifiers:
                    notification_channels.append(chat_id)
        notification_channels = list(set(notification_channels))  # вычищаем дубли
        if not notification_channels:
            notification_channels.append(CHAT_STRUCTURE.get('default'))

        # парсим сообщение в зависимости от источника
        message = make_web_solutions_message(alert) if is_web_solutions_alert else make_services_message(alert)

        # при неполадках с порталом отправляем СМС ответственному программисту
        if 'https://portal.keyauto.ru' in message:
            send_sms(
                PORTAL_PHONE_NUMBER,
                # MD не поддерживается в СМС
                message.replace('*', '').replace('`', '').replace('{', '').replace('}', '')
            )

        # отправляем сообщение во все выбранные каналы
        for chat_id in notification_channels:
            try:
                send_message(chat_id, message)
                return OK_MESSAGE, 200
            except RetryAfter:
                print('Telegram exception: RetryAfter')
                sleep(SMALL_TIMEOUT)
                send_message(chat_id, message)
                return OK_MESSAGE, 200
            except [
                TimedOut,
                NetworkError,
            ]:
                print('Telegram exception: TimedOut or NetworkError')
                sleep(LARGE_TIMEOUT)
                send_message(chat_id, message)
                print(f'Channel: {chat_id}, message: {message} send')
                return OK_MESSAGE, 200
            except Exception as error:
                print(f'Exception: {error}, channel: {chat_id}, message: {message}')
                APP.logger.info('\t%s', error)
                return FAIL_MESSAGE, 200


if __name__ == '__main__':
    basicConfig(level=INFO)
    APP.run(host=HOST, port=PORT)
