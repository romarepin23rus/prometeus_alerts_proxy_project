from datetime import datetime

from redis import Redis

from settings import DATETIME_FORMAT
from settings import REDIS_HOST
from settings import REDIS_PORT
from utils import encrypt_string


class RedisConnect:
    """
    Реализация коннекта с Redis.
    """
    def __init__(self):
        self._redis = Redis(
            host=REDIS_HOST,
            port=REDIS_PORT,
        )

    def read_value(self, service_key: str) -> dict:
        """
        Читаем значение по ключу service_key.
        :param service_key: Незашифрованный идентификатор сервиса.
        :return: Данные о последнем уведомлении о падении сервиса и количество его повторных падений.
        {
            'time_of_last_falling': datetime - время последнего падения,
            'repeated_crash_number': int - количество повторных падений сервиса,
        }
        """
        encrypted_service_key = encrypt_string(service_key)
        pipeline = self._redis.pipeline()
        pipeline.hget(encrypted_service_key, 'time_of_last_falling')
        pipeline.hget(encrypted_service_key, 'repeated_crash_number')
        record_data = pipeline.execute()
        if record_data[0] and record_data[1]:
            return {
                'time_of_last_falling': datetime.strptime(record_data[0].decode('utf-8'), DATETIME_FORMAT),
                'repeated_crash_number': int(record_data[1]),
            }
        else:
            return dict()

    def write_value(self, service_key: str, time_of_last_falling: datetime, repeated_crash_number: int) -> None:
        """
        Записываем значение record_value по ключу service_key.
        :param service_key: Незашифрованный идентификатор сервиса.
        :param time_of_last_falling: Время последнего уведомления о падении.
        :param repeated_crash_number: Количество повторных падений сервиса.
        :return: None.
        """
        encrypted_service_key = encrypt_string(service_key)
        pipeline = self._redis.pipeline()
        pipeline.hset(encrypted_service_key, 'time_of_last_falling', time_of_last_falling.strftime(DATETIME_FORMAT))
        pipeline.hset(encrypted_service_key, 'repeated_crash_number', str(repeated_crash_number))
        pipeline.execute()
