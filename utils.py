from datetime import datetime
from hashlib import md5

from requests import post
from requests.auth import HTTPBasicAuth

from settings import SMS_PASSWORD
from settings import SMS_URL
from settings import SMS_USER


def encrypt_string(unencrypted_test_en_name: str) -> str:
    """
    Шифруем строку при помощи md5.
    :param unencrypted_test_en_name: Незашифрованная строка.
    :return: Строка зашифрованная при помощи md5
    """
    return md5(unencrypted_test_en_name.encode('utf-8')).hexdigest()


def get_current_date_and_time() -> datetime:
    """
    Возвращает объект datetime с текущими датой и временем.

    :return: Текущие дата и время.
    """
    return datetime.now()


def send_sms(phone_number: str, message: str) -> None:
    """
    Отправляем СМС на указанный номер телефона.

    :param phone_number: Номер телефона в формате +7XXXXXXXXXX.
    :param message: Текст сообщения.
    :return: None.
    """
    basic_auth = HTTPBasicAuth(username=SMS_USER, password=SMS_PASSWORD)
    post(url=SMS_URL, data={'phone': phone_number, 'text': message}, verify=False, auth=basic_auth)
